#!/bin/sh
javahome='/home/misha/.jdks/openjdk-22.0.1'
export JAVA_HOME=${javahome}
M2_HOME='/snap/intellij-idea-community/502/plugins/maven/lib/maven3'
export M2_HOME
M2=${M2_HOME}/bin
export M2
mvn=${M2}/mvn

cd ../
#if [ -f "./target" ];
#  then rm -r ./target
#fi
${mvn} clean install

cd target || exit 1
java -jar that-sort.jar
