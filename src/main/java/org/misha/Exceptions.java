package org.misha;

public class Exceptions {
    class Common {
        Common() throws DetailException {
        }

        Number i(Number n) throws CommonException {
            return .3;
        }
    }

    class Detail extends Common {
        Detail() throws Exception {
            super();
        }

        @Override
        public Integer i(Number n) throws DetailException {
            return 1;
        }
    }

    class CommonException extends Exception {}

    class DetailException extends CommonException {}
}




