package org.misha.decorator;

public class EvenMoreWork extends WorkerDecorator {
    EvenMoreWork(final Worker worker) {
        super(worker);
    }

    @Override
    public int work() {
        return super.work() * 2;
    }
}
