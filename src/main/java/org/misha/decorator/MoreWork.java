package org.misha.decorator;

public class MoreWork extends WorkerDecorator {
    MoreWork(final Worker worker) {
        super(worker);
    }

    @Override
    public int work() {
        return super.work() + 1;
    }
}


