package org.misha.decorator;

public interface Worker {
    int work();
}
