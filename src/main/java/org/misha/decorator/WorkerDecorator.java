package org.misha.decorator;

public abstract class WorkerDecorator implements Worker {
    private final Worker worker;

    WorkerDecorator(final Worker worker) {
        this.worker = worker;
    }

    @Override
    public int work() {
        return worker.work();//no decoration
    }
}
