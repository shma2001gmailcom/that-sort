package org.misha.forkjoin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinSum {
    private final List<Integer> terms;

    public ForkJoinSum() {
        this.terms = new ArrayList<>();
    }

    public void addTerms(List<Integer> values) {
        terms.addAll(values);
    }

    public int sum(int parallelism) {
        ForkJoinPool poll = new ForkJoinPool(4);
        final ForkJoinSumTask forkJoinSumTask = new ForkJoinSumTask(terms, parallelism);
        poll.invoke(forkJoinSumTask);
        return forkJoinSumTask.compute();
    }
}
