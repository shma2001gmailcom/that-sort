package org.misha.forkjoin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

public class ForkJoinSumTask extends RecursiveTask<Integer> {
    private final List<Integer> terms;
    private final int parallelism;

    ForkJoinSumTask(List<Integer> terms, int parallelism) {
        this.terms = terms;
        this.parallelism = parallelism;
    }

    @Override
    protected Integer compute() {
        if (terms.size() > 2) {
            int sum = 0;
            for (RecursiveTask<Integer> integerRecursiveTask : ForkJoinTask.invokeAll(divide())) {
                int join = integerRecursiveTask.join();
                sum += join;
            }
            return sum;
        }
        return terms.stream().reduce(0, (a, b) -> a + b);
    }

    private Collection<RecursiveTask<Integer>> divide() {
        final List<RecursiveTask<Integer>> dividedTasks = new ArrayList<>();
        int size = terms.size();
        dividedTasks.add(new ForkJoinSumTask(terms.subList(0, size / 2), parallelism));
        dividedTasks.add(new ForkJoinSumTask(terms.subList(size / 2, size), parallelism));
        return dividedTasks;
    }
}
