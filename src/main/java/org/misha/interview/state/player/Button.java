package org.misha.interview.state.player;

/**
 * author: misha
 * date: 3/1/18
 */
public interface Button {
    void onClick();
}
