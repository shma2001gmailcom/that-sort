package org.misha.interview.state.player;

/**
 * author: misha
 * date: 3/1/18
 */
public class View {
    private final Button lockButton;
    private final Button playButton;
    private final Button nextButton;
    private final Button prevButton;

    public View(final Button lockButton, final Button playButton, final Button nextButton, final Button prevButton) {
        this.lockButton = lockButton;
        this.playButton = playButton;
        this.nextButton = nextButton;
        this.prevButton = prevButton;
    }

    public Button playButton() {
        return playButton;
    }

    public Button lockButton() {
        return lockButton;
    }

    public Button nextButton() {
        return nextButton;
    }

    public Button prevButton() {
        return prevButton;
    }
}
