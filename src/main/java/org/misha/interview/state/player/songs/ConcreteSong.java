package org.misha.interview.state.player.songs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author: misha
 * date: 3/2/18
 */
public class ConcreteSong extends Song {
    private final Logger log = LoggerFactory.getLogger(Song.class);

    ConcreteSong(final String name) {
        this.name = name;
    }

    @Override
    public void play() {
        log.debug(toString());
    }
}
