package org.misha.proxy.proxyjdk.impl;

import org.misha.proxy.proxyjdk.MyObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: mshevelin
 * Date: 2/2/15
 * Time: 4:07 PM
 */
@SuppressWarnings("unused class")
public final class RealObject implements MyObject {
    private static final Logger log = LoggerFactory.getLogger(RealObject.class);
    private final int count;
    private final String name;

    @SuppressWarnings("unused")
    private RealObject(final Integer times, final String name) {
        count = times;
        this.name = name;
    }

    @Override
    public void printCount() {
        log.info("{}", count);
    }

    @Override
    public void printName(final int times) {
        int c = 0;
        while (c < times) {
            c++;
            log.info(name);
        }
    }
}
