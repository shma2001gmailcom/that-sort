package org.misha.runtime.annotation.objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author: misha
 * date: 2/21/16 8:48 PM.
 */
public class Your {
    private static final Logger log = LoggerFactory.getLogger(Your.class);

    public void write() {
        log.info("your");
    }
}
