package org.misha.threads.waitnotify.waiter;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] a) {
        final Mutable<String> mutable = new Mutable<String>() {
            private String text;

            String getText() {
                return text;
            }

            void setText(String text) {
                this.text = text;
            }

            @Override
            public void mutate(String s) {
                setText(s);
            }

            @Override
            public String toString() {
                return text;
            }
        };
        IntStream.range(0, 1500).parallel().forEach(i -> new Thread(new Waiter<>(mutable), "waiter" + i).start());
        new Thread(new Notifier<>(mutable), "notifier").start();
        System.err.println("all waiter has been started");
    }
}
