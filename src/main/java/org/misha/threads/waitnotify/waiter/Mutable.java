package org.misha.threads.waitnotify.waiter;

public abstract class Mutable<T> {

    public abstract void mutate(T t);
}
