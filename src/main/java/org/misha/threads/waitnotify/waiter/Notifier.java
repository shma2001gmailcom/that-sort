package org.misha.threads.waitnotify.waiter;

import static java.lang.Thread.currentThread;

public class Notifier<T extends Mutable> implements Runnable {
    private final T sharedObj;

    public Notifier(T sharedObj) {
        this.sharedObj = sharedObj;
    }

    @Override
    public void run() {
        String name = currentThread().getName();
        System.err.println("started " + name);
        synchronized (sharedObj) {
            sharedObj.mutate(name);
            sharedObj.notifyAll();

        }

    }
}
