package org.misha.threads.waitnotify.waiter;

import static java.lang.Thread.currentThread;

public class Waiter<T extends Mutable> implements Runnable {
    private final T sharedObj;

    Waiter(T sharedObj) {
        this.sharedObj = sharedObj;
    }

    @Override
    public void run() {
        System.err.println("waiter: waiting for notification");
        String name = currentThread().getName();
        synchronized (sharedObj) {
            try {
                sharedObj.wait();
            } catch (InterruptedException e) {
                currentThread().interrupt();
                System.err.println("interrupted: " + name);
            }
            sharedObj.mutate(name);
            System.err.println("notify has been called. " + name + ":" + sharedObj);
        }
    }
}
