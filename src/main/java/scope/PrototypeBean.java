package scope;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.identityHashCode;

public class PrototypeBean {
    private static final Logger log = LoggerFactory.getLogger(PrototypeBean.class);
    private static final AtomicInteger count = new AtomicInteger();

    public PrototypeBean() {
        log.error("cnt: " + count + "; " + this);
        count.getAndIncrement();
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && identityHashCode(this) == identityHashCode(obj);
    }

    @Override
    public String toString() {
        return String.valueOf(identityHashCode(this));
    }
}
