package scope;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

@Component
public class PrototypeFactory implements FactoryBean<PrototypeBean> {

    private PrototypeBean makePrototype() {
        return new PrototypeBean();
    }

    @Override
    public PrototypeBean getObject() {
        return makePrototype();
    }

    @Override
    public Class<?> getObjectType() {
        return PrototypeBean.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }
}
