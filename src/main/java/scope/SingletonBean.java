package scope;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class SingletonBean {
    private final PrototypeFactory factory;
    private PrototypeBean prototypeBean;

    public SingletonBean(PrototypeFactory factory) {
        this.factory = factory;
    }

    Set<PrototypeBean> prototypes() {
        Set<PrototypeBean> set = new HashSet<>();
        for (int i = 0; i < 3; ++i) {
            this.prototypeBean = factory.getObject();
            set.add(prototypeBean);
        }
        return set;
    }
}
