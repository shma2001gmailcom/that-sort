package org.misha.decorator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main {
    @Test
    public void test() {
        //(0 + 1 + 1) * 2
        Worker w = new EvenMoreWork(new MoreWork(new MoreWork(new PoorWorker())));
        assertEquals(4, w.work());
    }
}
