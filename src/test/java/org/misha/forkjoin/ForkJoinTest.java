package org.misha.forkjoin;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForkJoinTest {

    @Test
    public void testSearch() {
        assertEquals(3, new ForkJoinSearch<>().search(
                newArrayList("c", "a", "c", "b", "c", "d"),
                "c"::equals
        ).size());
    }

    @Test
    public void testSearch0() {
        Pattern pattern = Pattern.compile("[0-9]+");
        assertEquals(0, new ForkJoinSearch<>().search(
                newArrayList(),
                "c"::equals
        ).size());
    }

    @Test
    public void testSum() {
        ForkJoinSum forkJoinSum = new ForkJoinSum();
        ArrayList<Integer> integers = new ArrayList<>();
        IntStream.range(0, 1000000).forEach(integers::add);
        forkJoinSum.addTerms(integers);
        System.err.println(forkJoinSum.sum(2));
    }
}