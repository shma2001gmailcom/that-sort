package org.misha.interview;

import org.junit.jupiter.api.Test;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.groupingBy;
import static org.misha.interview.Overlapping.range;

public class OverlappingTest {
    @Test
    public void test() {
        Overlapping o = new Overlapping(range(0, 6),
                range(3, 8),
                range(7, 9),
                range(10, 13),
                range(15, 18),
                range(15, 19),
                range(20, 55));
        System.out.println(o);
        System.out.println(o.merge());
    }

    @Test
    public void testGroup() {
        System.err.println(newArrayList(
                new Data(0, "Pit"),
                new Data(1, "Ann"),
                new Data(2, "Pit"),
                new Data(3, "Greg"),
                new Data(4, "Pit"),
                new Data(5, "Ann"),
                new Data(6, "Ann"),
                new Data(7, "Greg"),
                new Data(8, "Greg")
        ).stream().collect(groupingBy(Data::getName)).getClass());
    }
    static class Data {
        int num;
        String name;

        public Data(final int num, final String name) {
            this.num = num;
            this.name = name;
        }

        public int getNum() {
            return num;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return "{" +
                    "num=" + num +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}
