package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Given an array of integers, determine whether the array can be sorted in ascending order using only one of the
 * following operations one time.
 * Swap two elements.
 * Reverse one sub-segment.
 */
public class AlmostSortedTest {
    public static void almostSorted(List<Integer> arr) {
        SwapResult swapResult = checkSwap(arr);
        if (swapResult.swapCountRuleHolds && swapResult.swapRuleHolds) {
            System.out.println("yes\n" + swapResult.verdict);
        } else {
            GroupResult groupResult = checkGroup(arr);
            if (groupResult.groupCountRuleHolds && groupResult.groupRuleHolds) {
                System.out.println("yes\n" + groupResult.verdict);
            } else {
                System.out.println("no");
            }
        }
    }

    private static GroupResult checkGroup(List<Integer> arr) {
        Iterator<Integer> first = arr.iterator();
        Iterator<Integer> second = arr.iterator();
        int offset = 0;
        int right = first.next();
        int left;
        int gapCount = 0;
        int startGroup = -1;
        int endGroup = -1;
        int preGroup = 0;
        int postGroup = 0;
        while (second.hasNext()) {
            if (first.hasNext()) {
                right = first.next();
            }
            left = second.next();
            ++offset;
            if (right < left) {
                if (offset > 1) {
                    preGroup = arr.get(offset - 2);
                }
                startGroup = left;
                while (left > right) {
                    if (first.hasNext()) {
                        right = first.next();
                        ++offset;
                    }
                    left = second.next();
                }
                postGroup = right;
                endGroup = left;
                ++gapCount;
            }
        }
        return new GroupResult(gapCount <= 2, (startGroup < postGroup || endGroup == postGroup) && endGroup > preGroup,
                "reverse " + endGroup + " " + startGroup);
    }

    private static SwapResult checkSwap(List<Integer> arr) {
        Iterator<Integer> first = arr.iterator();
        Iterator<Integer> second = arr.iterator();
        int offset = 0;
        int right = first.next();
        int left;
        int start = -1;
        int end = -1;
        int trCount = 0;
        int preStart = -1;
        int postStart = -1;
        int preEnd = -1;
        int postEnd = -1;
        while (second.hasNext()) {
            if (first.hasNext()) {
                right = first.next();
            }
            left = second.next();
            ++offset;
            if (left > right) {
                if (trCount == 0) {
                    start = left;
                    if (offset - 2 >= 0) {
                        preStart = arr.get(offset - 2);
                    }
                    postStart = right;
                    if (offset + 1 < arr.size()) {
                        postEnd = arr.get(offset + 1);
                        end = right;
                    } else {
                        postEnd = start;
                        end = postStart;
                    }
                } else {
                    end = right;
                    if (offset + 1 < arr.size()) {
                        postEnd = arr.get(offset + 1);
                    } else {
                        postEnd = start + 1;
                    }
                    preEnd = left;
                }
                ++trCount;
            }
        }
        if (trCount == 1 && (postStart < preStart) || start > postEnd) {
            return new SwapResult(false, false, "no");
        }
        return new SwapResult(trCount <= 2, preEnd < start && preStart < end && end <= postStart,
                "swap " + (arr.indexOf(start) + 1) + " " + (arr.indexOf(end) + 1));
    }

    @Test
    public void test() {
        List<Integer> arr;
        arr = new ArrayList<Integer>() {{
            add(43);
            add(65);
            add(1);
            add(98);
            add(99);
            add(101);
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(1);
            add(7);//start group
            add(6);
            add(5);
            add(4);
            add(3);
            add(2);//end group
            add(8);
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(7);//start group
            add(6);
            add(5);
            add(4);
            add(3);
            add(2);
            add(1);//end group
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(4);//start group
            add(3);
            add(2);
            add(1);//end group
            add(5);
            add(6);
            add(7);
            add(8);
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(8);//start group
            add(7);
            add(6);
            add(5);
            add(4);
            add(3);//end group
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(4);//start swap
            add(2);//end swap
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(6);//start swap
            add(4);
            add(5);
            add(3);//end swap
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(1);
            add(2);
            add(3);
            add(4);
            add(6);//start swap
            add(5);//end swap
        }};
        almostSorted(arr);
        arr = new ArrayList<Integer>() {{
            add(2);//start swap
            add(1);//end swap
            add(3);
            add(4);
            add(5);
            add(6);
        }};
        almostSorted(arr);
    }
    static class SwapResult {
        boolean swapCountRuleHolds;
        boolean swapRuleHolds;
        String verdict;

        SwapResult(boolean swapCountRuleHolds, boolean swapRuleHolds, String verdict) {
            this.swapCountRuleHolds = swapCountRuleHolds;
            this.swapRuleHolds = swapRuleHolds;
            this.verdict = verdict;
        }

        @Override
        public String toString() {
            return "SwapResult{" + "swapCountRuleHolds=" + swapCountRuleHolds + ", swapRuleHolds=" + swapRuleHolds +
                    ", verdict='" + verdict + '\'' + '}';
        }
    }
    static class GroupResult {
        boolean groupCountRuleHolds;
        boolean groupRuleHolds;
        String verdict;

        GroupResult(boolean groupCountRuleHolds, boolean groupRuleHolds, String verdict) {
            this.groupCountRuleHolds = groupCountRuleHolds;
            this.groupRuleHolds = groupRuleHolds;
            this.verdict = verdict;
        }

        @Override
        public String toString() {
            return "GroupResult{" + "groupCountRuleHolds=" + groupCountRuleHolds + ", groupRuleHolds=" +
                    groupRuleHolds + ", verdict='" + verdict + '\'' + '}';
        }
    }
}
