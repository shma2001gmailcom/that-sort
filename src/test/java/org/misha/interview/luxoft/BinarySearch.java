package org.misha.interview.luxoft;

public class BinarySearch {

    private static int search(int[] arr, int low, int high, int target) {
        if (high > low) {
            final int mid = (high + low) / 2;
            final int val = arr[mid];
            if (target == val) {
                return mid;
            }
            if (target < val) {
                return search(arr, low, mid, target);
            }
            return search(arr, mid + 1, high, target);
        }
        return -1;
    }

    private static int searchTail(int[] arr, int target) {
        final int length = arr.length;
        int right = length;
        int left = 0;
        int mid = length / 2;
        for (int i = 0; i < 2 + Math.log(length); ++i) {
            if (arr[mid] == target) {
                return mid;
            }
            mid = (left + right) / 2;
            if (target < arr[mid]) {
                right = mid;
            }
            if (target > arr[mid]) {
                left = mid;
            }
        }
        return -1;
    }

    public static void main(String... args) {
        int[] x = {1, 7, 13, 18, 201, 313, 1001, 1101};
        System.err.println(search(x, 0, x.length, 201));
        System.err.println(searchTail(x, 1101));
        System.err.println(searchTail(x, 1));
    }
}
