package org.misha.interview.luxoft;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

//non-associative brackets  conf
public class Brackets {
    private static Set<Expression> next(Set<Expression> current) {
        final Set<Expression> result = new HashSet<>();
        for (Expression left : current) {
            for (Expression right : current) {
                result.add(new Expression(left, right));
                result.add(new Expression(right, left));
            }
        }
        return result;
    }

    public static void main(String[] a) {
        final Set<Expression> current = new TreeSet<>();
        current.add(new Leaf());
        for (int i = 0; i < 4; ++i) {//gc limit
            final Set<Expression> next = next(current);
            System.err.println(next + " " + next.size());
            current.addAll(next);
        }
    }
}

class Expression implements Comparable<Expression> {
    private Expression left;
    private Expression right;
    private int length;

    Expression() {
        this.length = 1;
    }

    Expression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
        this.length = left.length + right.length;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Expression that = (Expression) o;
        return Objects.equals(left, that.left) &&
                Objects.equals(right, that.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right);
    }

    @Override
    public String toString() {
        if (left.length == 1 && right.length == 1) {
            return left.toString() + right;
        }
        if (left.length == 1) {
            return left.toString() + "(" + right + ")";
        }
        if (right.length == 1) {
            return "(" + left + ")" + right;
        }
        return "(" + left.toString() + ")(" + right.toString() + ")";
    }

    @Override
    public int compareTo(@Nonnull final Expression e) {
        if (length == 1) return length - e.length;
        if (length == e.length) {
            return left.compareTo(e.left) == 0 ? right.compareTo(e.right) : left.compareTo(right);
        }
        return length - e.length;
    }
}

class Leaf extends Expression {
    Leaf() {
        super();
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(final Object obj) {
        return obj != null && obj.getClass().equals(Leaf.class);
    }

    @Override
    public String toString() {
        return "()";
    }
}
