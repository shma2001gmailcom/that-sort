package org.misha.interview.luxoft;

public class Catalan {
    private static int subsets(int setSize, int subsetSize) {
        if (setSize == 0) {
            return 0;
        }
        if (subsetSize == 0) {
            return 1;
        }
        if (setSize < subsetSize) return 0;
        if (setSize == subsetSize) return 1;
        return subsets(setSize - 1, subsetSize) + subsets(setSize - 1, subsetSize - 1);
    }

    private static long catalan(long n) {
        if (n < 3) {
            return 1;
        }
        int result = 0;
        final long oddness = n % 2;
        for (long i = 1; i < (n + oddness) / 2; ++i) {
            result += catalan(n - i) * catalan(i);
        }
        return oddness == 1 ? 2L * result : 2 * result + catalan(n / 2) * catalan(n / 2);
    }

    public static void main(String[] a) {
        for (int i = 1; i < 10; ++i) {
            System.err.println(i + ": " + catalan(i) + "=" + subsets(2 * (i - 1), (i - 1)) / i);
        }
    }
}
