package org.misha.interview.luxoft;
//print all numbers from the first sorted array not containing in the second sorted array

public class Differ {
    static void differ(int[] first, int[] second) {
        int f = 0;
        int s = 0;
        while (f < first.length && s < second.length) {
            int a = first[f];
            int b = second[s];
            if (a > b) {
                ++s;
            } else {
                if (a < b) {
                    System.out.println(a);
                    ++f;
                } else {
                    ++f;
                    ++s;
                }
            }
        }
        for (; f < first.length && first[f] != second[s - 1]; ++f) {
            System.out.println(first[f]);
        }
    }
}
