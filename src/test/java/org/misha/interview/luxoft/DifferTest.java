package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

class DifferTest {

    @Test
    void test() {
        int[] a = {1, 1, 2, 2, 3, 3, 4, 4, 5, 5};
        int[] b = {-2, 2, 2, 3, 3, 4, 4, 7, 7};
        Differ.differ(a, b);
        a = new int[]{1, 1, 1, 2, 3, 4, 6, 6, 6, 7};
        b = new int[]{1, 2, 5, 6, 6};
        Differ.differ(a, b);
    }
}
