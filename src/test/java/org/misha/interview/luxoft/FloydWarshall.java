package org.misha.interview.luxoft;

import static com.google.common.primitives.Ints.min;
import static java.lang.System.arraycopy;
import static java.lang.System.err;
import static java.lang.System.out;

public class FloydWarshall {
    private static final int INF_TY = 1000;
    private static final String INFINITY = "\u221E";
    private final int vCnt;
    private final int[][] distances;

    private FloydWarshall(final int vCnt) {
        this.vCnt = vCnt;
        distances = new int[vCnt][vCnt];
    }

    public static void main(String... args) {
        FloydWarshall fw = new FloydWarshall(4);
        fw.set(0, 1, 9);
        fw.set(0, 2, -4);
        fw.set(0, 3, INF_TY);
        fw.set(1, 0, 6);
        fw.set(1, 2, INF_TY);
        fw.set(1, 3, 2);
        fw.set(2, 0, INF_TY);
        fw.set(2, 1, 5);
        fw.set(2, 3, INF_TY);
        fw.set(3, 0, INF_TY);
        fw.set(3, 1, INF_TY);
        fw.set(3, 2, 1);
        err.println(fw);
        err.println(fw.findShortestPaths());
    }

    public void set(int row, int col, int val) {
        distances[row][col] = val;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("->\n");
        for (int row = 0; row < vCnt; ++row) {
            for (int col = 0; col < vCnt; ++col) {
                sb = sb.append(distances[row][col] == INF_TY ? INFINITY : distances[row][col]).append("\t");
            }
            sb.append('\n');
        }
        return sb.append("<-").toString();
    }

    private void doForVertex(int vNum) {
        int[][] next = new int[vCnt][vCnt];
        for (int row = 0; row < vCnt; ++row) {
            for (int col = 0; col < vCnt; ++col) {
                next[row][col] = min(distances[row][col], distances[row][vNum] + distances[vNum][col]);
            }
        }
        arraycopy(next, 0, distances, 0, vCnt);
        out.println("vertex: " + vNum + '\n' + this);
    }

    private FloydWarshall findShortestPaths() {
        for (int vertex = 0; vertex < vCnt; ++vertex) {
            doForVertex(vertex);
        }
        return this;
    }
}
