package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LaundryQueue {
    //It is known how long it takes a person standing in line at the laundry to wash.
    //There are washingMachines washing machines in the laundry room.
    //Calculate the amount of time required for all people to complete the work.
    private static int wash(final int washingMachines, final int... washingTimes) {//O(n*n)
        final List<Integer> people = new LinkedList<>();
        IntStream.of(washingTimes).forEach(people::add);//O(n)
        int result = 0;
        while (!people.isEmpty()) {
            final Integer time = people.get(minIndex(people, washingMachines));
            result += time;
            for (int i = 0; i < people.size() && i < washingMachines; people.set(i, people.get(i) - time), ++i) ;
            people.removeIf(i -> i == 0);
        }
        return result;
    }

    private static int wash0(int washingMachines, LinkedList<Integer> people, int result) {//O(n*n)
        if (people.isEmpty()) {
            return result;
        }
        final Integer time = people.get(minIndex(people, washingMachines));
        result += time;
        for (int i = 0; i < people.size() && i < washingMachines; people.set(i, people.get(i) - time), ++i) ;
        people.removeIf(i -> i == 0);
        return wash0(washingMachines, people, result);
    }

    private static int minIndex(List<Integer> list, int before) {
        int index = 0;
        int min = list.get(index);
        for (int i = 0; i < list.size() && i < before; i++) {
            final Integer k = list.get(i);
            if (min > k) {
                min = k;
                index = i;
            }
        }
        return index;
    }

    @Test
    public void testWash() {
        assertEquals(9, wash(3, 1, 2, 3, 4, 5, 6));
        assertEquals(9, wash0(3, new LinkedList<>(newArrayList(1, 2, 3, 4, 5, 6)), 0));
        assertEquals(11, wash(6, 9, 1, 11, 3, 8, 8, 8, 1, 2));
        assertEquals(11, wash0(6, new LinkedList<>(newArrayList(9, 1, 11, 3, 8, 8, 8, 1, 2)), 0));
    }
}
