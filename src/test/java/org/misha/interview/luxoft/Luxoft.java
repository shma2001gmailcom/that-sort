package org.misha.interview.luxoft;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;

import static java.lang.Math.min;
import static java.util.stream.Collectors.toList;

public class Luxoft {
    //second solution wins
    public static void main(String[] args) throws InterruptedException {
        int batchSize = 1;
        List<Integer> list = IntStream.range(1, 1000000).boxed().collect(toList());
        BatchIterator<Integer> it = new BatchIterator<>(list, batchSize);
        long start = System.currentTimeMillis();
        while (it.hasNext()) {
            it.next();
            //System.out.println(it.next());
        }
        long end = System.currentTimeMillis() - start;
        //sleep(100);
        BatchIterator1<Integer> it1 = new BatchIterator1<>(list, batchSize);
        long start1 = System.currentTimeMillis();
        while (it1.hasNext()) {
            it1.next();
            //System.err.println(it1.next());
        }
        long end1 = System.currentTimeMillis() - start1;
        System.err.println("first\n------- = " + (double) end / end1 + "\nsecond");
    }

    private static class BatchIterator<T> {
        private final int batchSize;
        private final Iterator<T> iterator;
        private final List<T> currentBatch;

        BatchIterator(Collection<T> collection, final int batchSize) {
            this.batchSize = batchSize;
            iterator = collection.iterator();
            currentBatch = new ArrayList<>();
        }

        boolean hasNext() {
            boolean notEmpty = false;
            for (int i = 0; i < batchSize; ++i) {
                if (iterator.hasNext()) {
                    notEmpty = true;
                    currentBatch.add(iterator.next());
                } else {
                    break;
                }
            }
            return notEmpty;
        }

        Collection<T> next() {
            Collection<T> next = new ArrayList<>();
            next.addAll(currentBatch);
            currentBatch.clear();
            return next;
        }
    }

    private static class BatchIterator1<T> {
        private final int batchSize;
        private final List<T> list;
        private int currentPosition;

        BatchIterator1(List<T> list, final int batchSize) {
            this.list = list;
            this.batchSize = batchSize;
        }

        boolean hasNext() {
            return currentPosition < list.size();
        }

        Collection<T> next() {
            final List<T> nextBatch = list.subList(currentPosition, min(currentPosition + batchSize, list.size()));
            currentPosition += min(batchSize, list.size() - currentPosition);
            return nextBatch;
        }
    }
}
