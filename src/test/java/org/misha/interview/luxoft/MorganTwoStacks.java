package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;

import static java.util.stream.Collectors.toList;

public class MorganTwoStacks {
    private static String morganAndString(String a, String b) {
        StringBuilder sb = new StringBuilder();
        LinkedList<Character> left = new LinkedList<>(a.chars().mapToObj(x -> (char) x).collect(toList()));
        LinkedList<Character> right = new LinkedList<>(b.chars().mapToObj(x -> (char) x).collect(toList()));
        char leftFirst;
        char rightFirst;
        while (!left.isEmpty() && !right.isEmpty()) {
            leftFirst = left.peek();
            rightFirst = right.peek();
            if (leftFirst == rightFirst) {
                onEqual(sb, left, right);
            } else if (leftFirst < rightFirst) {
                sb.append(left.pop());
            } else {
                sb.append(right.pop());
            }
        }
        for (char c : left) {
            sb.append(c);
        }
        for (char c : right) {
            sb.append(c);
        }
        return sb.toString();
    }

    private static void onEqual(StringBuilder sb, LinkedList<Character> left, LinkedList<Character> right) {
        char leftCurrent = left.get(0);
        char rightCurrent = right.get(0);
        int i = 0;
        while (leftCurrent == rightCurrent && i < left.size() && i < right.size()) {
            leftCurrent = left.get(i);
            rightCurrent = right.get(i);
            ++i;
        }
        if (leftCurrent < rightCurrent) {
            sb.append(left.pop());
        } else {
            sb.append(right.pop());
        }
    }

    @Test
    public void test() {
        String left = "ABACADCBA";
        String right = "ABACAABAB";
        System.err.println(morganAndString(left, right));
    }
}
