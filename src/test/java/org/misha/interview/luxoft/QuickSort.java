package org.misha.interview.luxoft;

import java.util.Arrays;

public class QuickSort {
    public static void sort(int[] array, int left, int right) {
        int pivot = array[(right + left) / 2];
        int b = left;
        int e = right;
        while (b <= e) {
            while (array[b] < pivot) ++b;
            while (array[e] > pivot) --e;
            if (b <= e) {
                swap(array, b, e);
                ++b;
                --e;
            }
        }
        if (left < e) sort(array, left, e);
        if (b < right) sort(array, b, right);
    }

    private static void swap(final int[] array, final int b, final int e) {
        int temp = array[b];
        array[b] = array[e];
        array[e] = temp;
    }

    public static void main(String[] args) {
        int[] a = {6, 6, 9, 1, 6, 6};
        sort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }
}
