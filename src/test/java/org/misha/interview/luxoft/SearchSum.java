package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchSum {
    //in an array find two integers which sum is sum
    private static String searchSum(int[] a, int sum) {//O(n)
        final Map<Integer, int[]> map = new HashMap<>();
        for (int key : a) {//O(n)
            map.computeIfPresent(key, (k, v) -> new int[]{sum - k, ++map.get(k)[1]});
            map.computeIfAbsent(key, k -> new int[]{sum - k, 1});
        }
        for (Map.Entry<Integer, int[]> e : map.entrySet()) {//O(n)
            final int val = e.getValue()[0];
            if (map.get(val) != null && (e.getKey() != val || e.getValue()[1] > 1)) {
                return e.getKey() + " " + val;
            }
        }
        return "no";
    }

    @Test
    public void testSearchSum() {
        int[] a = {4, 5, 5, 3};
        assertEquals("3 5", searchSum(a, 8));
        a = new int[]{1, 1, 1};
        assertEquals("no", searchSum(a, 1));
        assertEquals("1 1", searchSum(a, 2));
        assertEquals("no", searchSum(new int[]{1}, 1));
        assertEquals("1 1", searchSum(new int[]{1, 1, 2}, 2));
    }
}
