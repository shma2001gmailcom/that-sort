package org.misha.interview.luxoft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class SecondMax {
    private static final Logger log = LoggerFactory.getLogger("");

    private static int secondMax(int[] array) {
        int length = array.length;
        if (length < 2) {
            return Integer.MIN_VALUE;
        }
        int max0 = array[0];
        int max1 = array[1];
        if (length == 2) {
            return min(max0, max1);
        }
        int a = max1;
        int b = max0;
        max0 = max(a, b);
        max1 = min(a, b);
        for (int i = 2; i < length; ++i) {
            int current = array[i];
            if (current > max0) {
                max1 = max0;
                max0 = current;
            } else if (max1 < current && current < max0) {
                max1 = current;
            }
        }
        return max1;
    }

    private static int secondMax2(int[] array) {
        int length = array.length;
        if (length < 2) {
            return Integer.MIN_VALUE;
        }
        int a = array[0];
        int b = array[length - 1];
        int max0 = max(a, b);
        int max1 = min(a, b);
        for (int start = 0, end = length - 1; start < end; ++start, --end) {
            int m0 = max(array[start], array[end]);
            max0 = max(max0, m0);
            if (m0 != max0) {
                max1 = max(max1, m0);
            }
        }
        return max1;
    }

    private static int min(int a, int b) {
        return a >= b ? b : a;
    }

    private static int max(int a, int b) {
        return -min(-a, -b);
    }

    public static void main(String[] args) {
        log.error("{}", secondMax(new int[]{4, 7, 5, 11, 13, 3, 1, 8, 13, 11, 0}));
        log.error("{}", secondMax2(new int[]{4, 7, 5, 11, 13, 3, 1, 8, 13, 11, 0}));
    }
}
