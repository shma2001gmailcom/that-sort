package org.misha.interview.luxoft;

import java.util.LinkedList;

public class StackJava {
    public static void main(String... args) {
        Stack s = new Stack();
        s.add(8);
        s.add(5);
        s.add(8);
        s.add(11);
        s.add(15);
        s.add(18);
        s.add(18);
        System.err.println(s.pop());
        System.err.println(s.pop());
        System.err.println(s.max());
        System.err.println(isPalindrome("123321"));
        System.err.println(isPalindrome("123326"));
    }

    private static boolean isPalindrome(String s) {
        char[] chars = s.toCharArray();
        int length = chars.length;
        for (int i = 0, j = length - 1; i < j; ++i, --j) {
            if (chars[i] != chars[j]) {
                return false;
            }
        }
        return true;
    }
    static class Stack {
        private final LinkedList<Integer> list;
        private final LinkedList<Integer> maxList;

        Stack() {
            list = new LinkedList<>();
            maxList = new LinkedList<>();
            maxList.add(Integer.MIN_VALUE);
        }

        void add(int i) {
            if (i >= maxList.getLast()) {
                maxList.add(i);
            }
            list.add(i);
        }

        int pop() {
            Integer last = list.removeLast();
            int i = last;
            if (i == maxList.getLast()) {
                maxList.removeLast();
            }
            return last;
        }

        int max() {
            return maxList.getLast();
        }
    }
}
