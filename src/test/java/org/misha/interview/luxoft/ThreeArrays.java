package org.misha.interview.luxoft;

public class ThreeArrays {
    //find minimal common value of three sorted integer arrays
    private static int findCommon(int[] first, int[] second, int[] third) {
        if (first.length == 0 || second.length == 0 || third.length == 0) {
            throw new IllegalStateException();
        }
        int i = 0;
        int j = 0;
        int k = 0;
        int f = first[0];
        int s = second[0];
        int t = third[0];
        int min = min(f, s, t);
        while (!same(f, s, t) && i < first.length && j < second.length && k < third.length) {
            if (f == min && i < first.length - 1) {
                f = first[++i];
            } else if (s == min && j < second.length - 1) {
                s = second[++j];
            } else if (t == min && k < third.length - 1) {
                t = third[++k];
            } else {
                throw new IllegalStateException();
            }
            min = min(f, s, t);
        }
        return min;
    }

    private static int min(int i, int j) {
        return i < j ? i : j;
    }

    private static int min(int i, int j, int k) {
        return min(i, min(j, k));
    }

    private static boolean same(int i, int j, int k) {
        return i == j && j == k;
    }

    public static void main(String[] a) {
        int[] x = {1, 2, 4, 5};
        int[] y = {3, 3, 4};
        int[] z = {2, 3, 4, 5, 6};
        System.err.println(findCommon(x, y, z));
    }
}
