package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Larry has been given a permutation of a sequence of natural numbers incrementing from 1 as an array.
 * He must determine whether the array can be sorted using the following operation any number of times:
 * 3-circle permutation
 */
public class ThreeHyphenCircle {

    private int independentCircles(List<Integer> integers) {
        Set<List<Integer>> circles = new HashSet<>();
        Map<Integer, Integer> permutation = new HashMap<>();
        for (int i = 0; i < integers.size(); ++i) {
            permutation.put(i + 1, integers.get(i));
        }
        Set<Integer> checked = new HashSet<>();
        int a = 1;
        List<Integer> circle = new ArrayList<>();
        while (!integers.isEmpty()) {
            while (checked.add(a)) {
                circle.add(a);
                a = permutation.get(a);
            }
            if (!circle.isEmpty()) {
                circles.add(circle);
            }
            circle = new ArrayList<>();
            if (!integers.isEmpty()) {
                a = integers.remove(0);
            }
        }
        int result = 0;
        for (List<Integer> c : circles) {
            result += c.size() - 1;
        }
        return result % 2;
    }

    @Test
    public void test() {
        List<Integer> integers = new ArrayList<Integer>() {{
            add(2);
            add(1);
            add(4);
            add(3);
            add(6);
            add(5);
        }};
        assertEquals(1, independentCircles(integers));
        integers = new ArrayList<Integer>() {{
            add(3);
            add(6);
            add(1);
            add(4);
            add(2);
            add(5);
        }};
        assertEquals(1, independentCircles(integers));
        integers = new ArrayList<Integer>() {{
            add(2);
            add(3);
            add(1);
            add(5);
            add(6);
            add(4);
        }};
        assertEquals(0, independentCircles(integers));
    }
}
