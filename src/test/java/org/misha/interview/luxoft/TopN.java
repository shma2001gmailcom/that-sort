package org.misha.interview.luxoft;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static java.lang.Integer.MIN_VALUE;

/**
 * Given infinite stream of integers.
 * Using only JDK library implement class which would process those
 * integers
 * and at any moment will be able to provide N unique biggest values
 * among all processed.
 * Class interface:
 * 1. N is class constructor argument, immutable.
 * 2. void push(int val) - process integers one-by-one.
 * 3. Collection<Integer> top().
 */
class TopN {
    private final int n;
    private final LinkedList<Integer> maxList;
    private final Integer[] maxArr;

    TopN(int maxSize) {
        this.n = maxSize;
        maxList = new LinkedList<>();
        maxArr = new Integer[n];
        int i = 0;
        while (i < n) {
            maxArr[i] = MIN_VALUE;
            ++i;
        }
    }

    private static void insertList(int a, List<Integer> list, int start, int end) {
        int mid = (end + start) / 2;
        Integer pivot = list.get(mid);
        if (mid == start) {
            if (a < list.get(mid)) {
                if (mid == 0) {
                    list.add(0, a);
                } else {
                    list.add(mid - 1, a);
                }
            } else if (a > list.get(mid)) {
                if (mid == list.size() - 1) {
                    list.add(a);
                } else {
                    list.add(mid + 1, a);
                }
            }
        } else {
            if (a < pivot) {
                insertList(a, list, start, mid);
            } else if (a > pivot) {
                insertList(a, list, mid, end);
            }
        }
    }

    private static void insertArr(int a, Integer[] list, int start, int end) {
        int mid = (end + start) / 2;
        Integer pivot = list[mid];
        if (mid == start) {
            if (a < pivot && mid != 0) {
                insertA(list, mid, a);
            } else if (a > pivot) {
                int length = list.length;
                if (mid == length - 1) {
                    insertA(list, length - 1, a);
                } else {
                    insertA(list, mid, a);
                }
            }
        } else {
            if (a < pivot) {
                insertArr(a, list, start, mid);
            }
            if (a > pivot) {
                insertArr(a, list, mid, end);
            }
        }
    }

    static void insertA(Integer[] array, int position, int element) {
        if (position < 0 || position >= array.length) {
            return;
        }
        int i = 0;
        while (i < position) {
            array[i] = array[i + 1];
            ++i;
        }
        array[position] = element;
    }

    void push(int a) {
        if (maxList.isEmpty()) {
            maxList.push(a);
        } else if (a > maxList.getLast()) {
            maxList.add(a);
        } else {
            int size = maxList.size();
            for (int i = 0; i < size; ++i) {//O(n)
                if (a < maxList.get(i) && i != 0 && a > maxList.get(i - 1)) {
                    maxList.add(i, a);
                    break;
                }
            }
        }
        pop();
    }

    void pushListBinary(int a) {
        if (maxList.isEmpty()) {
            maxList.add(a);
        }
        if (maxList.size() == 1) {
            if (a > maxList.get(0)) {
                maxList.add(a);
            } else if (a < maxList.get(0)) {
                maxList.addFirst(a);
            }
        } else {
            insertList(a, maxList, 0, maxList.size());
            pop();
        }
    }

    void pushArrayBinary(int a) {
        if (maxArr[n - 1] == MIN_VALUE) {
            maxArr[n - 1] = a;
        } else if (n > 1 && maxArr[n - 2] == MIN_VALUE) {
            if (a > maxArr[n - 1]) {
                insertA(maxArr, n - 1, a);
            } else if (a < maxArr[n - 1]) {
                maxArr[n - 2] = a;
            }
        } else {
            insertArr(a, maxArr, 0, maxArr.length);
        }
    }

    List<Integer> topArray() {
        List<Integer> result = new ArrayList<>();
        int i = 0;
        while (i < n) {
            result.add(maxArr[i]);
            ++i;
        }
        return result;
    }

    private void pop() {
        int size = maxList.size();
        if (size > n) {
            maxList.pop();
        }
    }

    Collection<Integer> top() {
        return maxList;
    }
}



