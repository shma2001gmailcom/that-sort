package org.misha.interview.luxoft;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static org.misha.interview.luxoft.TopN.insertA;

public class TopNTest {
    private final List<Integer> integers;
    private final int n = 4;

    {
        integers = new ArrayList<>();
        IntStream.range(0, 100000).forEach(i -> integers.add(new Random().nextInt()));
    }

    @Test
    public void testInsert() throws Exception {
        Integer[] a = new Integer[]{3, 1, 7};
        insertA(a, 1, 9);
        System.out.println(a[0] + " " + a[1] + " " + a[2]);
    }

    @Test
    public void testPushCorrect() throws Exception {
        TopN topN = new TopN(n);
        printPush(topN, 3);
        printPush(topN, 7);
        printPush(topN, 2);
        printPush(topN, 6);
        printPush(topN, 1);
        printPush(topN, 8);
        printPush(topN, 7);
        printPush(topN, 6);
        printPush(topN, 9);
    }

    @Test
    public void testPush1Correct() throws Exception {
        TopN topN = new TopN(n);
        printPush1(topN, 3);
        printPush1(topN, 7);
        printPush1(topN, 2);
        printPush1(topN, 6);
        printPush1(topN, 1);
        printPush1(topN, 8);
        printPush1(topN, 7);
        printPush1(topN, 6);
        printPush1(topN, 9);
    }

    @Test
    public void testPushACorrect() throws Exception {
        TopN topN = new TopN(n);
        printPushA(topN, 3);
        printPushA(topN, 7);
        printPushA(topN, 2);
        printPushA(topN, 6);
        printPushA(topN, 1);
        printPushA(topN, 8);
        printPushA(topN, 7);
        printPushA(topN, 6);
        printPushA(topN, 9);
        printPushA(topN, 3);
    }

    @Test
    public void push1() throws Exception {
        push1Time();
    }

    private void push1Time() {
        long start = System.currentTimeMillis();
        TopN topN = new TopN(1111);
        integers.forEach(topN::pushListBinary);
        System.out.println("pushListBinary time: " + (System.currentTimeMillis() - start));
    }

    private void pushTime() {
        long start = System.currentTimeMillis();
        TopN topN = new TopN(1111);
        integers.forEach(topN::push);
        System.out.println("push time: " + (System.currentTimeMillis() - start));
    }

    private void pushATime() {
        long start = System.currentTimeMillis();
        TopN topN = new TopN(1111);
        integers.forEach(topN::pushArrayBinary);
        System.out.println("pushArrayBinary time: " + (System.currentTimeMillis() - start));
    }

    @Test
    public void compareTime() throws Exception {
        pushATime();//30 ms
        push1Time();//600 ms
        pushTime();//79598
    }

    private void printPush(TopN topN, int a) {
        topN.push(a);
        System.out.println("pushing " + a);
        System.out.println(topN.top());
    }

    private void printPush1(TopN topN, int a) {
        topN.pushListBinary(a);
        System.out.println("pushing " + a);
        System.out.println(topN.top());
    }

    private void printPushA(TopN topN, int a) {
        topN.pushArrayBinary(a);
        System.out.println("pushing " + a);
        System.out.println(topN.topArray());
    }
}


