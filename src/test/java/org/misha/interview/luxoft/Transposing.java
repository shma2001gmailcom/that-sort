package org.misha.interview.luxoft;

import java.util.ArrayList;
import java.util.List;

import static org.misha.interview.luxoft.MatrixRotation.print;

//https://www.hackerrank.com/challenges/matrix-rotation-algo/problem effort #1 (simply but too slow)
public class Transposing {
    public static void main(String[] args) {
        List<List<Integer>> matrix = new ArrayList<List<Integer>>() {{
            add(new ArrayList<Integer>() {{
                add(11);
                add(12);
                add(13);
            }});
            add(new ArrayList<Integer>() {{
                add(21);
                add(22);
                add(23);
            }});
            add(new ArrayList<Integer>() {{
                add(31);
                add(32);
                add(33);
            }});
        }};
        int temp, tempNext;
        for (int i = 0; i < matrix.size(); ++i) {
            for (int j = 0; j < matrix.get(0).size(); ++j) {
                if (i != j) {
                    temp = matrix.get(i).get(j);
                    tempNext = matrix.get(j).get(i);
                    matrix.get(i).set(j, tempNext);
                    matrix.get(j).set(i, temp);
                }
            }
        }
        print(matrix);
    }
}
