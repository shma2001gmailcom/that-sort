package org.misha.state.player;

import org.misha.interview.state.player.Player;
import org.misha.interview.state.player.songs.PlayList;
import org.misha.interview.state.player.songs.Song;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * author: misha
 * date: 3/1/18
 */
class MockPlayer {
    static Player makePlayer() {
        Player player = new Player();
        player.setPlayList(new PlayList() {{
            put(new Song() {

                private final Logger log = LoggerFactory.getLogger(Song.class);

                @Override
                public void play() {
                    log.debug(toString());
                }

                {
                    setName("All you need is love");
                }
            });
            put(new Song() {

                private final Logger log = LoggerFactory.getLogger(Song.class);

                @Override
                public void play() {
                    log.debug(toString());
                }

                {
                    setName("Us and them");
                }
            });
        }});
        return player;
    }
}
