package org.misha.state.player;

import org.junit.jupiter.api.Test;
import org.misha.interview.state.player.Player;

import static org.misha.state.player.MockPlayer.makePlayer;

/**
 * author: misha
 * date: 3/1/18
 */
public class PlayerTest {
    private Player player;

    PlayerTest() {
        player = makePlayer();
    }

    private void startPlayback() {
        player.view.playButton().onClick();
    }

    private void stopPlayback() {
        player.view.lockButton().onClick();
    }

    private void nextSong() {
        player.view.nextButton().onClick();
    }

    private void previousSong() {
        player.view.prevButton().onClick();
    }

    @Test
    public void testPlayer() {
        startPlayback();
        stopPlayback();
        nextSong();
        previousSong();
        stopPlayback();
        startPlayback();
        previousSong();
        nextSong();
        stopPlayback();
    }
}