package scope;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringJUnitConfig(classes = {PrototypeFactory.class, SingletonBean.class})
public class SingletonBeanTest {

    @Autowired
    SingletonBean singletonBean;

    @Test
    public void testScope() {
        Set<PrototypeBean> prototypes = singletonBean.prototypes();
        assertEquals(3, prototypes.size());
    }
}